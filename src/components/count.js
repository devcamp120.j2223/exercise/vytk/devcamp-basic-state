import { Component } from "react";

class Count extends Component{
    constructor(props){
        super(props);
        this.state={
            count:this.props.init
        }
        //this.onBtnClick=this.onBtnClick.bind(this);
    }
    onBtnClick=()=>{
        this.setState({
            count:this.state.count+1
        })
    }

    render(){
        return(
            <div>
                <p>Count Click button: {this.state.count}</p>
                <button onClick={this.onBtnClick}>Click me!</button>
            </div>
        )
    }
}
export default Count;