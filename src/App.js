import Count from "./components/count";

function App() {
  return (
    <div >
      <Count init={1}/>
    </div>
  );
}

export default App;
